# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.contrib.admin.templatetags.admin_list import search_form
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect
from django.http import JsonResponse

# Create your views here.
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, CreateView
from django.views.generic import ListView, DetailView

from adverts.forms import CreateNewAdvertForm
from adverts.models import CreateNewAdvert, Media
from fermanew import settings
from main.models import Banners


class AdvertListView(ListView):
    model = CreateNewAdvert
    context_object_name = 'all_ads'
    # paginate_by = 10
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(AdvertListView, self).get_context_data(**kwargs)
        all_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True))
        sell_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True, advert_type='sell'))
        buy_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True, advert_type='buy'))
        change_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True, advert_type='change'))
        rent_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True, advert_type='rent'))
        offer_ads = sort_adds(CreateNewAdvert.objects.filter(is_active=True, advert_type='offers'))
        context['all_ads'] = paginate(self.request, all_ads)['result']
        context['sell_ads'] = paginate(self.request, sell_ads)['result']
        context['buy_ads'] = paginate(self.request, buy_ads)['result']
        context['change_ads'] = paginate(self.request, change_ads)['result']
        context['rent_ads'] = paginate(self.request, rent_ads)['result']
        context['offer_ads'] = paginate(self.request, offer_ads)['result']
        if self.request.GET.get('category'):
            filtered = all_ads.filter(is_active=True,
                                      categories=self.request.GET.get('category'))
            context['all_ads'] = paginate(self.request, filtered)['result']
        elif self.request.GET.get('search_words'):
            filtered = all_ads.filter(
                text__icontains=self.request.GET.get('search_words'))
            context['all_ads'] = paginate(self.request, filtered)['result']
        return context


class AdCreate(CreateView):
    form_class = CreateNewAdvertForm
    success_url = "/"
    template_name = 'new_advert.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            new_ad = CreateNewAdvert()
            new_ad.title = form.cleaned_data['title']
            new_ad.name = form.cleaned_data['name']
            new_ad.email = form.cleaned_data['email']
            new_ad.advert_type = form.cleaned_data['advert_type']
            new_ad.categories = form.cleaned_data['categories']
            new_ad.text = form.cleaned_data['text']
            new_ad.agree_with_rules = True
            new_ad.is_active = False
            new_ad.save()
            if form.cleaned_data['removed_images']:
                removed_images = form.cleaned_data['removed_images'].split(',')
                for item in removed_images:
                    try:
                        r_media = Media.objects.get(id=int(item))
                        file_path = settings.MEDIA_ROOT + '/' + r_media.media_file.name
                        os.remove(file_path)
                        r_media.delete()
                    except ObjectDoesNotExist:
                        pass
            if form.cleaned_data['images']:
                images = form.cleaned_data['images'].split(',')
                for item in images:
                    try:
                        media = Media.objects.get(id=int(item))
                        new_ad.media.add(media)
                    except ObjectDoesNotExist:
                        pass
            return redirect('/')
        else:
            return self.form_invalid(form)


class AdDetail(DetailView):
    model = CreateNewAdvert
    template_name = 'one_ad_page.html'
    context_object_name = 'ad'


def copy_items(request):
    for i in range(10):
        obj = CreateNewAdvert.objects.get(pk=22)
        obj.pk = None
        obj.save()

    return JsonResponse(dict(sucess=True))


def sort_adds(obj):
    result = list()
    banner = Banners.objects.all()
    counter = 0
    adds_counter = 1
    if banner:
        for i in obj:
            if adds_counter % 4 != 0:
                result.append(i)
                adds_counter += 1
            else:
                if counter + 1 <= len(banner):
                    result.append(banner[counter])
                    adds_counter += 1
                    counter += 1
                else:
                    counter = 0
                    result.append(banner[counter])
                    counter += 1
                    adds_counter += 1
    else:
        result = obj
    return result


@csrf_exempt
def upload_media(request):
    uploaded_files = list()
    files_count = len(request.FILES)
    for i in range(0, files_count):
        _file = request.FILES['file-' + str(i)]
        media = Media()
        media.media_file = _file
        media.save()
        uploaded_files.append({'id': media.id, 'url': media.media_file.url})
    return JsonResponse(dict(uploaded_files=uploaded_files))


@csrf_exempt
def remove_uploaded_media(request):
    if request.POST:
        ids = request.POST.get('media_ids')
        ids = ids.split(',')
        for item in ids:
            try:
                media = Media.objects.get(id=int(item))
                file_path = settings.MEDIA_ROOT + '/' + media.media_file.name
                os.remove(file_path)
                media.delete()
            except ObjectDoesNotExist:
                pass

        return JsonResponse(dict(done=True))
    return JsonResponse(dict(done=False))


def paginate(request, queryset):
    page = request.GET.get('page', 1)

    paginator = Paginator(queryset, 12)
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    context = {'result': result}
    return context
