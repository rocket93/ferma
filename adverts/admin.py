from django.contrib import admin

# Register your models here.
from adverts.models import CreateNewAdvert, Media


class AdvertAdmin(admin.ModelAdmin):
    list_display = ('title', 'email', 'is_active',)
    list_editable = ('is_active',)
    readonly_fields = ('agree_with_rules',)


admin.site.register(CreateNewAdvert, AdvertAdmin)
admin.site.register(Media)
