# coding=utf-8
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from adverts.models import ad_type, category_type


class MainSlider(models.Model):
    class Meta:
        verbose_name_plural = 'Картинки для слайдера'
        verbose_name = 'Картинка для слайдера'

    image = models.ImageField(upload_to='slider_images', verbose_name='Изображение')
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=255, verbose_name='Краткое описание')

    def __str__(self):
        return self.title


class Banners(models.Model):
    class Meta:
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'

    image = models.ImageField(upload_to='banners', verbose_name='Баннер')

    def __str__(self):
        return str(self.image)


class Subscribers(models.Model):
    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'

    email = models.EmailField(verbose_name='Email пользователя', unique=True)
    advert_type = models.CharField(max_length=255, verbose_name='Тип объявления',
                                   choices=ad_type, null=True, blank=True)
    categories = models.CharField(max_length=255, verbose_name='Категория',
                                  choices=category_type, null=True, blank=True)
    is_active = models.BooleanField(default=True, verbose_name='Подписчик активен?')

    def __str__(self):
        return self.email
