from django.http import JsonResponse
from django.shortcuts import redirect
from main.forms import SubscriberForm
from main.models import MainSlider, Subscribers


def slider(request):
    slider = MainSlider.objects.all()
    context = {
        'slider': slider
    }
    return context


def create_subscriber(request):
    form = SubscriberForm(request.POST)
    context = {
        'subscriber_form': form
    }
    if request.POST:
        if form.is_valid():
            email = form.cleaned_data['email']
            ad_type = form.cleaned_data['advert_type']
            category = form.cleaned_data['categories']
            subscriber = Subscribers()
            subscriber.email = email
            subscriber.advert_type = ad_type
            subscriber.categories = category
            subscriber.save()
            return JsonResponse(dict(success=True))
    return context
