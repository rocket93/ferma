from django import forms

from main.models import Subscribers


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscribers
        exclude = ('is_active',)

    def __init__(self, *args, **kwargs):
        super(SubscriberForm, self).__init__(*args, **kwargs)
        self.fields['advert_type'].required = False
        self.fields['categories'].required = False
