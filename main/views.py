# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView

from adverts.models import CreateNewAdvert
from companies.models import Companies
from main.forms import SubscriberForm
from main.models import MainSlider, Subscribers
# Create your views here.
from news.models import News

