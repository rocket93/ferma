# coding=utf-8
from __future__ import unicode_literals
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models


# Create your models here.
from django.template.defaultfilters import slugify


class News(models.Model):
    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    preview = models.ImageField(upload_to='news_images/', verbose_name='Превью новости', null=True,
                                blank=True)
    title = models.CharField(max_length=255, verbose_name='Название')
    text = RichTextUploadingField()
    date = models.DateField(auto_now_add=True)
    slug = models.SlugField(null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.slug:
            self.slug = slugify(self.title)
        super(News, self).save()

    def __str__(self):
        return self.title
