# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import threading

# Create your views here.
from django.core.mail import EmailMessage
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import JsonResponse, request
from django.template import loader
from django.views.generic import *

from adverts.models import CreateNewAdvert
from fermanew import settings
from main.models import Subscribers
from news.models import News


class NewsListView(ListView):
    model = News
    context_object_name = "news_ad"
    paginate_by = 5
    template_name = 'news_list.html'


class NewsDetailView(DetailView):
    model = News
    context_object_name = 'news'
    template_name = 'one_news.html'
    slug_field = 'slug'


@receiver(post_save, sender=CreateNewAdvert, dispatch_uid="subscriber_notify")
def send_notify(sender, instance, **kwargs):
    if instance.is_active == True:
        for i in Subscribers.objects.filter(is_active=True):
            email = i.email
            t = loader.get_template('partial/_notify_email.html')
            c = dict(advert=instance, BASE_URL=settings.BASE_URL,)
            letter = t.render(c)
            thread = threading.Thread(target=send_notification_email,
                                      args=('Новое сообщение', letter, email))
            thread.start()
        return JsonResponse(dict(sucess=True, message='Yes'))
    else:
        return JsonResponse(dict(sucess=False, message='No'))


def send_notification_email(title, body, to):
    email = EmailMessage(title, body=body, to=[to])
    email.content_subtype = 'html'
    email.send()
